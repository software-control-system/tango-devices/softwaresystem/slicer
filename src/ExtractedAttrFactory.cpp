#include <ExtractedAttrFactory.h>
#include <tango.h>
#include <Slicer.h>
#include <SlicerClassExt.h>

namespace Slicer_ns
{

  vector<string> ExtractedAttrFactory::splitString(string input, const char separator)
{
	vector<string> strings;
	istringstream iss(input);
    string word;
    while ( std::getline( iss, word, separator ) )
    {
        strings.push_back(word);
    }
	return strings;
}

ExtractedAttr* ExtractedAttrFactory::getAttribute(string name)
{
  int j=0;
	while(attributes[j]->getDisplayName() != name)
	{				 
		j++;
	}
  return attributes[j];
}

ExtractedAttr* ExtractedAttrFactory::createAttribute(string inputPath, string outputName, string indexesCouple)
{
	Tango::Attr* lIndex = 0;
	Tango::Attr* cIndex = 0;

	try
	{
		vector<string> attributeIndexes = splitString(indexesCouple, ',');

		int id = (device->get_device_attr()->get_attr_nb()-2); // -2 : state, status

		if(attributeIndexes.size() == 2)
		{
			std::istringstream issLine(attributeIndexes[0]);
			long longLine;
			issLine >> longLine;

			std::istringstream issColumn(attributeIndexes[1]);
			long longColumn;
			issColumn >> longColumn;

      Tango::AttributeProxy* proxy = new Tango::AttributeProxy(inputPath);

      // Dynamically creating a spectrum OR a scalar attribute
			Tango::DeviceAttribute result;
			result = proxy->read();

			// For scientific users : a spectrum's dimensions are supposed to be y>1 and x=1. In Tango world, spectrum's dimensions
			// are x>0 and y=0.
			// What are we doing here ? if it looks like the user got confused between the two previous representation, we switch the indexes.
			// !! Only if the attribute is a spectrum !!
			if((result.get_dim_y() == 0) && (longLine != -1) && (longColumn <= 0))
			{
				long tmp = longLine;
				longLine = longColumn;
				longColumn = tmp;
			}

      DEBUG_STREAM << "longLine: " << longLine << ", longColumn: " << longColumn << endl;

       // Creating extracted values READ attribute
			if((result.get_dim_y() > 0) && ((longColumn== -1) || (longLine == -1)))
			{
        DEBUG_STREAM << "Creating a spectrum..." << endl;
        ExtractedSpectrumAttr* extracted_values = 0;
        try
        {
				  extracted_values = new ExtractedSpectrumAttr(device, id, outputName, inputPath, longLine, longColumn);
          if(extracted_values != 0)
			    {
            extracted_values->setProxy(proxy);

			      // Creating indexs READ_WRITE attributes
			      lIndex = new LineIndexAttr(extracted_values);
			      if(lIndex != 0)
			      {
				      Tango::UserDefaultAttrProp	lIndex_prop;
				      lIndex_prop.set_label((outputName+std::string("LIndex")).c_str());
				      lIndex_prop.set_description((std::string("LineIndex for attribute ")+inputPath).c_str());
				      lIndex->set_default_properties(lIndex_prop);
				      device->add_attribute(lIndex);
			      }
			      else
			      {
				      throw std::bad_alloc();
			      }

			      cIndex = new ColumnIndexAttr(extracted_values);
			      if(cIndex != 0)
			      {
				      Tango::UserDefaultAttrProp	cIndex_prop;
				      cIndex_prop.set_label((outputName+std::string("CIndex")).c_str());
				      cIndex_prop.set_description((std::string("ColumnIndex for attribute ")+inputPath).c_str());
				      cIndex->set_default_properties(cIndex_prop);
				      device->add_attribute(cIndex);
			      }
			      else
			      {
				      throw std::bad_alloc();
			      }

				    Tango::UserDefaultAttrProp	extracted_values_prop;
				    extracted_values_prop.set_label(outputName.c_str());
				    extracted_values_prop.set_description((std::string("Values extracted by the slicer from ")+inputPath).c_str());
				    extracted_values->set_default_properties(extracted_values_prop);
				    device->add_attribute(extracted_values);
				    attributes.push_back(extracted_values);
			    }
			    else
			    {
				    throw std::bad_alloc();
			    }
        }
        catch(std::bad_alloc)
	      {
		      extracted_values->setRunning(false);
		      ERROR_STREAM << "Initialization Error [allocation of attribute "+inputPath+" failed]" << endl;
	      }
	      catch (Tango::ConnectionFailed& ex) 
	      {
		      extracted_values->setRunning(false);
	      }
	      catch(Tango::DevFailed& ex)
	      {
		      extracted_values->setRunning(false);
	      }
        return extracted_values;
			}
			else
			{
        DEBUG_STREAM << "Creating a scalar..." << endl;
        ExtractedScalarAttr* extracted_values = 0;
        try
        {
				  extracted_values = new ExtractedScalarAttr(device, id, outputName, inputPath, longLine, longColumn);
          if(extracted_values != 0)
			    {
            extracted_values->setProxy(proxy);

			      // Creating indexs READ_WRITE attributes
			      lIndex = new LineIndexAttr(extracted_values);
			      if(lIndex != 0)
			      {
				      Tango::UserDefaultAttrProp	lIndex_prop;
				      lIndex_prop.set_label((outputName+std::string("LIndex")).c_str());
				      lIndex_prop.set_description((std::string("LineIndex for attribute ")+inputPath).c_str());
				      lIndex->set_default_properties(lIndex_prop);
				      device->add_attribute(lIndex);
			      }
			      else
			      {
				      throw std::bad_alloc();
			      }

			      cIndex = new ColumnIndexAttr(extracted_values);
			      if(cIndex != 0)
			      {
				      Tango::UserDefaultAttrProp	cIndex_prop;
				      cIndex_prop.set_label((outputName+std::string("CIndex")).c_str());
				      cIndex_prop.set_description((std::string("ColumnIndex for attribute ")+inputPath).c_str());
				      cIndex->set_default_properties(cIndex_prop);
				      device->add_attribute(cIndex);
			      }
			      else
			      {
				      throw std::bad_alloc();
			      }

				    Tango::UserDefaultAttrProp	extracted_values_prop;
				    extracted_values_prop.set_label(outputName.c_str());
				    extracted_values_prop.set_description((std::string("Values extracted by the slicer from ")+inputPath).c_str());
				    extracted_values->set_default_properties(extracted_values_prop);
				    device->add_attribute(extracted_values);
				    attributes.push_back(extracted_values);
			    }
			    else
			    {
				    throw std::bad_alloc();
			    }
        }
        catch(std::bad_alloc)
	      {
		      extracted_values->setRunning(false);
		      ERROR_STREAM << "Initialization Error [allocation of attribute "+inputPath+" failed]" << endl;
	      }
	      catch (Tango::ConnectionFailed& ex) 
	      {
		      extracted_values->setRunning(false);
	      }
	      catch(Tango::DevFailed& ex)
	      {
		      extracted_values->setRunning(false);
	      }
        return extracted_values;
      } // end of if((result.get_dim_y() > 0) && ((longColumn== -1) || (longLine == -1)))


    } // end of if(attributeIndexes.size() == 2)
  } // end of TRY
	catch(std::bad_alloc)
	{
		//anOutput->setRunning(false);
		ERROR_STREAM << "Initialization Error [allocation of attribute "+inputPath+" failed]" << endl;
	}
	catch (Tango::ConnectionFailed& ex) 
	{
		//anOutput->setRunning(false);
	}
	catch(Tango::DevFailed& ex)
	{
		//anOutput->setRunning(false);
	}
  return 0;
}
}

 
