static const char *RcsId = "$Header: /users/chaize/newsvn/cvsroot/Process/Slicer/src/ClassFactory.cpp,v 1.1.1.1 2006-01-19 16:11:58 syldup Exp $";
//+=============================================================================
//
// file :        ClassFactory.cpp
//
// description : C++ source for the class_factory method of the DServer
//               device class. This method is responsible to create
//               all class singletin for a device server. It is called
//               at device server startup
//
// project :     TANGO Device Server
//
// $Author: syldup $
//
// $Revision: 1.1.1.1 $
//
// $Log: not supported by cvs2svn $
//
// copyleft :     Synchrotron SOLEIL
//                L'Orme des Merisiers
//                Saint-Aubin - BP 48
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================


#include <tango.h>
#include <SlicerClass.h>

/**
 *	Create SlicerClass singleton and store it in DServer object.
 */

void Tango::DServer::class_factory()
{

	add_class(Slicer_ns::SlicerClass::init("Slicer"));

}
