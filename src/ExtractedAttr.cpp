#include <ExtractedAttr.h>
#include <tango.h>
#include <Slicer.h>

namespace Slicer_ns
{
 ExtractedAttr::ExtractedAttr(Tango::DeviceImpl* host_device, int id, string name, string path, long lIndex, long cIndex):Tango::LogAdapter(host_device), device(host_device)
 {
	lastReadValues = 0;

	displayName = name;

	pathToProxy = path;	
	
	running = false;

	lineIndex_write = lIndex;
	lineIndex_read = new Tango::DevLong;
	*lineIndex_read = lIndex;

	columnIndex_write = cIndex;
	columnIndex_read = new Tango::DevLong;
	*columnIndex_read = cIndex;

	attributeId = id;
}

void ExtractedAttr::setProxy(Tango::AttributeProxy* proxy)
{
	proxyToData = proxy; 
	//INFO_STREAM << "Connected to " << path << endl;
	serverAttributeInfo = proxyToData->get_config();
	//DEBUG_STREAM << "Attribute type : " << serverAttributeInfo.data_type << endl; 
	running = true;
}

ExtractedAttr::~ExtractedAttr()
{
	if(lastReadValues)
	{
		delete lastReadValues;
		lastReadValues = 0;
	}

	if(proxyToData)
	{
		delete proxyToData;
		proxyToData=0;
	}
}

int ExtractedAttr::getData()
{
	// values extracted from the values read from the server attribute
	int nbValuesExtracted = 0;
	
	Tango::DeviceAttribute result;
	
	// First, check the connection by reading the attribute
	try
	{
		INFO_STREAM << "About to read attribute " << this->getDisplayName() << endl;
		result = this->getProxy()->read();
	}
	catch (Tango::DevFailed& ex) 
	{
		TangoSys_OMemStream o;
		o << "Connection failed [Could not read from " << this->getDisplayName() << "]" << ends;
		LOG_ERROR((o.str()));

    if(device->dev_state() == Tango::RUNNING)
		{
			this->setRunning(false);
		}

		Tango::Except::re_throw_exception(ex,
			(const char *)"TANGO_WRONG_DATA_ERROR",
			o.str(),
			(const char *)"Slicer::getData");
	}

	// Checking the display type of ExtractedValues
	checkDisplay(result.get_dim_y());

	DEBUG_STREAM << "Checking was done" << endl;
	
	// Read was successful --> Extract the data
	try
	{
		switch(this->getServerAttributeInfo().data_type)
		{
			case Tango::DEV_SHORT:
				{
					vector<Tango::DevShort> tempValues;
					nbValuesExtracted = extractData(tempValues, result);
				}
				break;
			case Tango::DEV_LONG:
				{
					vector<Tango::DevLong> tempValues;
					nbValuesExtracted = extractData(tempValues, result);
				}
				break;
			case Tango::DEV_FLOAT:
				{
					vector<Tango::DevFloat> tempValues;
					nbValuesExtracted = extractData(tempValues, result);
				}
				break;
			case Tango::DEV_DOUBLE:
				{
					vector<Tango::DevDouble> tempValues;
					nbValuesExtracted = extractData(tempValues, result);
				}
				break;
			case Tango::DEV_BOOLEAN:
				{
					vector<Tango::DevBoolean> tempValues;
					nbValuesExtracted = extractData(tempValues, result);
				}
				break;
			case Tango::DEV_UCHAR:
				{
					vector<Tango::DevUChar> tempValues;
					nbValuesExtracted = extractData(tempValues, result);
				}
				break;
			case Tango::DEV_USHORT:
				{
					vector<Tango::DevUShort> tempValues;
					nbValuesExtracted = extractData(tempValues, result);
				}
				break;
			default:
				{
					TangoSys_OMemStream o;
					o << "Extraction error [Unsupported data type]. Hit : if first initialization failed (because ther server was not running) and you just launched the server, you have to call Init method" << ends;
					LOG_ERROR((o.str()));
					this->setRunning(false);
					Tango::Except::throw_exception(
					(const char *)"TANGO_WRONG_DATA_ERROR",
					o.str(),
					(const char *)"Slicer::getData");
					return 0;
				}
				break;
		}

	}
	catch (Tango::DevFailed& ex) 
	{
		TangoSys_OMemStream o;
		o << "Extraction error [DevFailed]" << ends;
		LOG_ERROR((o.str()));
		if(device->dev_state() == Tango::RUNNING) // Not to cover "Unsupported data type" status
		{
			this->setRunning(false);
		}
		Tango::Except::re_throw_exception(ex,
			(const char *)"TANGO_WRONG_DATA_ERROR",
			o.str(),
			(const char *)"Slicer::getData");
	}
	catch(...)
	{
		TangoSys_OMemStream o;
		o << "Extraction error [Unknown error]" << ends;
		LOG_ERROR((o.str()));
		this->setRunning(false);
		Tango::Except::throw_exception(
			(const char *)"UNKNOWN_ERROR",
			o.str(),
			(const char *)"Slicer::getData");
	}

	return nbValuesExtracted;
}

void ExtractedAttr::checkDisplay(int dimY)
{
	// Exceptions are thrown until the client switch the values back to a configuration that fit the original configuration
	
	DEBUG_STREAM << "scalar= " << this->isScalar() << ", this->getLineIndex()=" << *this->getLineIndex() << ", this->getColumnIndex()=" << *this->getColumnIndex() << endl;
	
	if(this->isScalar()) 
	{	
		if((dimY > 0) && ((*this->getLineIndex() == -1) || (*this->getColumnIndex() == -1)))
		{
			// Slicer was extracting a single value (scalar) from a matrix
			// From now Slicer has to extract a spectrum
			//display_error=true;
			// throw exception
			TangoSys_OMemStream o;
			o << "Device was set up to extract a scalar, but it's not the case anymore." << ends;
			LOG_ERROR((o.str()));

			this->setRunning(false);
			Tango::Except::throw_exception(
				(const char *)"TANGO_WRONG_DATA_ERROR",
				o.str(),
				(const char *)"Slicer::checkDisplay");
		}
		/*else
		{
			if(display_error)
			{
				display_error=false;
			}
		}*/
	}
	else
	{
		// Slicer was extracting a spectrum (thus from a matrix)
		if((*this->getLineIndex() != -1) && (*this->getColumnIndex() != -1))
		{
			// From now Slicer will extract a single value
			//display_error=true;
			// throw exception
			TangoSys_OMemStream o;
			o << "Device was set up to extract a spectrum, but it's not the case anymore." << ends;
			LOG_ERROR((o.str()));

			this->setRunning(false);
			Tango::Except::throw_exception(
				(const char *)"TANGO_WRONG_DATA_ERROR",
				o.str(),
				(const char *)"Slicer::checkDisplay");
		}
		/*else
		{
			if(display_error)
			{
				display_error=false;
			}
		}*/
	}
}

template <typename T>
int ExtractedAttr::extractData(vector<T> serverData, Tango::DeviceAttribute serverReadAttribute)
{
	int nbValuesExtracted = 0;
	int dimX = serverReadAttribute.get_dim_x();
	int dimY = serverReadAttribute.get_dim_y();

	if(	this->getLastReadValues() != 0 )
	{
		delete this->getLastReadValues();
		this->setLastReadValues(0);
	}

	if((serverReadAttribute >> serverData) == true)
	{
		int nbValues = serverData.size();
		DEBUG_STREAM << "DimX=" << dimX << ", DimY=" << dimY << ", NbValues=" << nbValues << endl;

		INFO_STREAM << "Extracting data ..." << endl;


		// Dimension checking : checking if the wanted value(s) belong(s) to the produced data
		if(*this->getColumnIndex() >= dimX)
		{		
			TangoSys_OMemStream o;
			o << "Extraction error [Out of range : ColumnIndex(" << *this->getColumnIndex() << ") >= DimX(" << dimX << ")]" << ends;
			LOG_ERROR((o.str()));

			this->setRunning(false);
			Tango::Except::throw_exception(
				(const char *)"DATA_OUT_OF_RANGE",
				o.str(),
				(const char *)"Slicer::extractData");

			return 0;
		}

		if(((dimY > 0) && (*this->getLineIndex() >= dimY)) || ((dimY == 0) && (*this->getLineIndex() > 1)))
		{		
			TangoSys_OMemStream o;
			o << "Extraction error [Out of range : LineIndex(" << *this->getLineIndex() << ") >= DimY(" << dimY << ") ]" << ends;
			LOG_ERROR((o.str()));

			this->setRunning(false);
			Tango::Except::throw_exception(
				(const char *)"DATA_OUT_OF_RANGE",
				o.str(),
				(const char *)"Slicer::extractData");

			return 0;
		}

		// Extraction
		try
		{
			if((*this->getLineIndex() >= 1) && (*this->getColumnIndex() == -1))
			{
				// Extraction of a line
					INFO_STREAM << " About to extract a line" << endl;
					// Initialization of attr_ExtractedValues_read
					this->setLastReadValues(new Tango::DevDouble[dimX]);
					::memset(this->getLastReadValues(), 0, dimX * sizeof(Tango::DevDouble));
					// Extracting the data
					for(int i=0; i<dimX; i++)
					{
						this->getLastReadValues()[i] = serverData[(*this->getLineIndex()-1)*dimX+i];
						nbValuesExtracted++;
					}
			}
			else if((*this->getLineIndex() == -1) && (*this->getColumnIndex() >= 1))
			{
				if(dimY != 0)
				{
					// Extraction of a column	
						INFO_STREAM << " About to extract a column" << endl;
						// Initialization of attr_ExtractedValues_read
						this->setLastReadValues(new Tango::DevDouble[dimY]);
						::memset(this->getLastReadValues(), 0, dimY * sizeof(Tango::DevDouble));
						// Extracting the data
						for(int i=0; i<dimY; i++)
						{
							this->getLastReadValues()[i] = serverData[(*this->getColumnIndex()-1)+dimX*i];
							nbValuesExtracted++;
						}
				}
				else
				{
					// Extracting a single value from a single-dimension matrix (ie a vector)
						INFO_STREAM << " About to extract a single value from a vector" << endl;
						// Initialization of attr_ExtractedValues_read
						this->setLastReadValues(new Tango::DevDouble[1]);
						::memset(this->getLastReadValues(), 0, sizeof(Tango::DevDouble));
						// Extracting the data
						this->getLastReadValues()[0] = serverData[*this->getColumnIndex()-1];
						nbValuesExtracted++;
				}
			}
			else if((*this->getLineIndex() >= 1) && (*this->getColumnIndex() >= 1))
			{
				// Extracting a single value from a matrix
				INFO_STREAM << "About to extract a single value from a matrix" << endl;
				// Initialization of attr_ExtractedValues_read
				this->setLastReadValues(new Tango::DevDouble[1]);
				::memset(this->getLastReadValues(), 0, sizeof(Tango::DevDouble));
				// Extracting the data
				this->getLastReadValues()[0] = serverData[(*this->getLineIndex()-1)*dimX+(*this->getColumnIndex()-1)];
				nbValuesExtracted++;
			}
			else
			{		
				// Wrong index configuration : *attr_LineIndex_read=-1 and *attr_ColumnIndex_read=-1
					TangoSys_OMemStream o;
          if((*this->getLineIndex() == -1) && (*this->getColumnIndex() == -1))
          {
					  o << "Extraction error [Wrong indexes configuration - Hint: at least one index should be >= 1]" << ends;
					  LOG_ERROR((o.str()));
          }
          else
          {
					  o << "Extraction error [Wrong indexes configuration - Hint: index value 0 is not a correct value. Indexes start with 1 value.]" << ends;
					  LOG_ERROR((o.str()));
          }

					this->setRunning(false);
					Tango::Except::throw_exception(
						(const char *)"TANGO_WRONG_DATA_ERROR",
						o.str(),
						(const char *)"Slicer::extractData");
					return 0;
			}

		}
		catch (Tango::CommunicationFailed& ex) 
		{
			TangoSys_OMemStream o;
			o << "Extraction error [Connection lost]" << ends;

			this->setRunning(false);
			LOG_ERROR((o.str()));
			Tango::Except::throw_exception(
				(const char *)"TANGO_COMMUNICATION_ERROR",
				o.str(),
				(const char *)"Slicer::extractData");
		}
		catch (Tango::DevFailed& ex) 
		{
		
			TangoSys_OMemStream o;
			o << "Extraction error [ DimX=" << dimX << ", ColumnIndex=" << *this->getColumnIndex() << " ,DimY=" << dimY << ", LineIndex=" << *this->getLineIndex() << "]" << ends;
			if(device->dev_state() == Tango::RUNNING) // Not to cover "Wrong indexes configuration" status
			{
				this->setRunning(false);
			}
			LOG_ERROR((o.str()));
			Tango::Except::re_throw_exception(ex,
				(const char *)"TANGO_WRONG_DATA_ERROR",
				o.str(),
				(const char *)"Slicer::extractData");
		}
		catch(...)
		{

			this->setRunning(false);
			TangoSys_OMemStream o;
			o << "Unknown error" << ends;
			LOG_ERROR((o.str()));
			Tango::Except::throw_exception(
				(const char *)"UNKNOWN_ERROR",
				o.str(),
				(const char *)"Slicer::extractData");
		}
	}
	else
	{
		this->setRunning(false);
		Tango::Except::throw_exception(
		(const char *)"TANGO_WRONG_DATA_ERROR",
		(const char *)"Error when reading the server attribute" ,
		(const char *)"Slicer::getData");
	}
	INFO_STREAM << nbValuesExtracted << " value(s) were extracted." << endl;
	return nbValuesExtracted;


}


}


