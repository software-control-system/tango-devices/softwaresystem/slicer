#ifndef _EXTRACTEDATTR_H
#define _EXTRACTEDATTR_H

#include <tango.h>

namespace Slicer_ns
{

class ExtractedAttr : public Tango::LogAdapter
{
protected:
	int attributeId;
	Tango::DevDouble* lastReadValues;
	int lastReadNbValues;
	// Proxy to device
	string pathToProxy;
	Tango::AttributeProxy* proxyToData;
	// We don't want to use the network every single time we need to get the properties of the
	// server's attribute
	Tango::AttributeInfo serverAttributeInfo;
	// Explicit output name
	string displayName;
	// Scalar or spectrum
	bool scalar;
	// State
	bool running;
	// indexes
	Tango::DevLong* columnIndex_read;
	Tango::DevLong* lineIndex_read;
	Tango::DevLong columnIndex_write;
	Tango::DevLong lineIndex_write;

  Tango::DeviceImpl* device;

public:
	ExtractedAttr(Tango::DeviceImpl* host_device, int id, string name, string path, long lIndex, long cIndex);
	~ExtractedAttr();

	Tango::DevDouble* getLastReadValues() {return lastReadValues;}
	int getLastReadNbValues() {return lastReadNbValues;}
	Tango::AttributeProxy* getProxy() {return proxyToData;}
	string getDisplayName(){return displayName;}
	Tango::AttributeInfo getServerAttributeInfo() {return serverAttributeInfo;}
	bool isScalar() {return scalar;}
	bool isRunning() {return running;}
	string getPathToProxy() {return pathToProxy;}
	Tango::DevLong* getColumnIndex() {return columnIndex_read;}
	Tango::DevLong* getLineIndex() {return lineIndex_read;}
	int getAttributeId() {return attributeId;}

	void setScalar(bool value) {scalar = value;}
	void setLastReadValues(Tango::DevDouble* values) {lastReadValues = values;}
	void setLastReadNbValues(int nbValues) {lastReadNbValues = nbValues;}
	void setRunning(bool state) {running = state;}
	void setPathToProxy(string path) {pathToProxy = path;}
	void setLineIndex(Tango::DevLong index) {lineIndex_write=index; *lineIndex_read = index; }
	void setColumnIndex(Tango::DevLong index) {columnIndex_write=index; *columnIndex_read=index;}

  Tango::DeviceImpl* getDevice() {return device;}

	void setProxy(Tango::AttributeProxy* proxy);

  	// Called everytime a client tries to read ExtractedValues
	int getData();

	// Called in getData() to extract specific values from the global data extracted from the server
	template <typename T>
		int extractData(vector<T> serverData, Tango::DeviceAttribute serverReadAttribute);

	// Check if the current display is the correct one
	void checkDisplay(int dimY);
};

class ExtractedScalarAttr: public ExtractedAttr, public Tango::Attr
{
public:
  ExtractedScalarAttr(Tango::DeviceImpl* host_device, int id, string name, string path, long lIndex, long cIndex):
     ExtractedAttr(host_device, id, name, path, lIndex, cIndex),
     Tango::Attr(name.c_str(), Tango::DEV_DOUBLE, Tango::READ)
     { setScalar(true); }

  virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{ att.set_value(this->getLastReadValues(), this->getLastReadNbValues());}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return true;}
};

class ExtractedSpectrumAttr: public ExtractedAttr, public Tango::SpectrumAttr
{
public:
  ExtractedSpectrumAttr(Tango::DeviceImpl* host_device, int id, string name, string path, long lIndex, long cIndex):
  ExtractedAttr(host_device, id, name, path, lIndex, cIndex),
  SpectrumAttr(name.c_str(), Tango::DEV_DOUBLE, Tango::READ, 4096)
  { setScalar(false); }

  virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{ att.set_value(this->getLastReadValues(), this->getLastReadNbValues());}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return true;}
};


}

#endif // _EXTRACTEDATTR_H
