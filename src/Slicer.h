//=============================================================================
//
// file :        Slicer.h
//
// description : Include for the Slicer class.
//
// project :	SlicerProject
//
// $Author: vince_soleil $
//
// $Revision: 1.7 $
//
// $Log: not supported by cvs2svn $
// Revision 1.6  2006/05/05 10:33:28  siraut
// Fixed multiple instances bug
// Fixed non linux compatibility
//
// Revision 1.5  2006/01/26 09:50:47  siraut
// Updated documentation
//
// Revision 1.4  2006/01/25 13:35:25  siraut
// - DynamicAttributesFactory -> ExtractedAttrFactory
// - Properties -> vector<string>
// - Indexes commencent � 1
//
// Revision 1.3  2006/01/25 09:13:59  siraut
// Changed design (ExtractedAttr, ColumnIndexAttr, ...)
//
// Revision 1.2  2006/01/20 16:30:22  siraut
// Changed design! Included DynamicAttributesFactory, DynamicAttribute, ...
//
// Revision 1.1.1.1  2006/01/19 16:11:58  syldup
// initial import
//
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================
#ifndef _SLICER_H
#define _SLICER_H

#include <tango.h>
#include <ExtractedAttrFactory.h>

#pragma warning( disable : 4101 ) 
//using namespace Tango;

/**
 * @author	$Author: vince_soleil $
 * @version	$Revision: 1.7 $
 */

 //	Add your own constants definitions here.
 //-----------------------------------------------


namespace Slicer_ns
{

/**
 * Class Description:
 * A device which extracts some numerical data from the data produced (stored in one or several attributes) by other devices
 */

/*
 *	Device States Description:
 *	Tango::FAULT :	Wrong properties configuration
 *	Tango::RUNNING :	No extraction problems
 *	Tango::ALARM :	If the extraction failed on at least one attribute.
 */


class Slicer: public Tango::Device_4Impl
{
public :
	//	Add your own data members here
	//-----------------------------------------


	//	Here is the Start of the automatic code generation part
	//-------------------------------------------------------------	
/**
 *	@name attributes
 *	Attributs member data.
 */
//@{
//@}

/**
 *	@name Device properties
 *	Device properties member data.
 */
//@{
/**
 *	Defines the attributes the slicer should extract data from
 */
	vector<string>	inputAttributes;
/**
 *	Defines the names of the dynamically created outputs
 */
	vector<string>	outputNames;
/**
 *	List of (LineIndex, ColumnIndex) which specify which data the slicer should extract from each of the attributes
 */
	vector<string>	indexes;
//@}

/**@name Constructors
 * Miscellaneous constructors */
//@{
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	Slicer(Tango::DeviceClass *cl,string &s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	Slicer(Tango::DeviceClass *cl,const char *s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device name
 *	@param d	Device description.
 */
	Slicer(Tango::DeviceClass *cl,const char *s,const char *d);
//@}

/**@name Destructor
 * Only one desctructor is defined for this class */
//@{
/**
 * The object desctructor.
 */	
	~Slicer() {delete_device();};
/**
 *	will be called at device destruction or at init command.
 */
	void delete_device();
//@}

	
/**@name Miscellaneous methods */
//@{
/**
 *	Initialize the device
 */
	virtual void init_device();
/**
 *	Always executed method befor execution command method.
 */
	virtual void always_executed_hook();

  virtual void read_attr_hardware(vector<long> &attr_list);

//@}

/**
 * @name Slicer methods prototypes
 */

//@{
/**
 * This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
 *	@return	State Code
 *	@exception DevFailed
 */
	virtual Tango::DevState	dev_state();

/**
 *	Read the device properties from database
 */
	 void get_device_property();
//@}

	//	Here is the end of the automatic code generation part
	//-------------------------------------------------------------	
  void delete_dynamic_attributes();


protected :	
	//	Add your own data members here
	//-----------------------------------------
  ExtractedAttrFactory* factory;

	vector<string> splitString(string input, const char separator);
	bool stringContains(string value, string content);

	string connectedTo;

  int nbAttributes;
};

}	// namespace

#endif	// _SLICER_H
