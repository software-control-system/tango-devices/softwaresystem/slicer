#ifndef _OUTPUT_H
#define _OUTPUT_H

#include <tango.h>

namespace Slicer_ns
{

class Output
{

private:
	int attributeId;
	Tango::DevDouble* lastReadValues;
	int lastReadNbValues;
	// Proxy to device
	string pathToProxy;
	Tango::AttributeProxy* proxyToData;
	// We don't want to use the network every single time we need to get the properties of the
	// server's attribute
	Tango::AttributeInfo serverAttributeInfo;
	// Explicit output name
	string displayName;
	// Scalar or spectrum
	bool scalar;
	// State
	bool running;
	// indexes
	Tango::DevLong* columnIndex_read;
	Tango::DevLong* lineIndex_read;
	Tango::DevLong columnIndex_write;
	Tango::DevLong lineIndex_write;




public:
	Output(int id, string name, string path, long lIndex, long cIndex);
	~Output();

	Tango::DevDouble* getLastReadValues() {return lastReadValues;}
	int getLastReadNbValues() {return lastReadNbValues;}
	Tango::AttributeProxy* getProxy() {return proxyToData;}
	string getDisplayName(){return displayName;}
	Tango::AttributeInfo getServerAttributeInfo() {return serverAttributeInfo;}
	bool isScalar() {return scalar;}
	bool isRunning() {return running;}
	string getPathToProxy() {return pathToProxy;}
	Tango::DevLong* getColumnIndex() {return columnIndex_read;}
	Tango::DevLong* getLineIndex() {return lineIndex_read;}
	int getAttributeId() {return attributeId;}

	void setScalar(bool value) {scalar = value;}
	void setLastReadValues(Tango::DevDouble* values) {lastReadValues = values;}
	void setLastReadNbValues(int nbValues) {lastReadNbValues = nbValues;}
	void setRunning(bool state) {running = state;}
	void setPathToProxy(string path) {pathToProxy = path;}
	void setLineIndex(Tango::DevLong index) {lineIndex_write=index; *lineIndex_read = index; }
	void setColumnIndex(Tango::DevLong index) {columnIndex_write=index; *columnIndex_read=index;}

	bool createProxy();
};

}

#endif // _OUTPUT_H


