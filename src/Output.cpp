#include <Output.h>
#include <tango.h>
#include <Slicer.h>

namespace Slicer_ns
{
Output::Output(int id, string name, string path, long lIndex, long cIndex)
{
	lastReadValues = 0;

	displayName = name;

	pathToProxy = path;	
	
	running = false;

	lineIndex_write = lIndex;
	lineIndex_read = new Tango::DevLong;
	*lineIndex_read = lIndex;

	columnIndex_write = cIndex;
	columnIndex_read = new Tango::DevLong;
	*columnIndex_read = cIndex;

	attributeId = id;
}

bool Output::createProxy()
{
	// Getting the proxy. Exceptions are caught in Slicer.cpp (Init)
	proxyToData = new Tango::AttributeProxy(pathToProxy);
	if(proxyToData == 0)
	{
		throw std::bad_alloc();
	}
	else
	{
		//INFO_STREAM << "Connected to " << path << endl;
		serverAttributeInfo = proxyToData->get_config();
		//DEBUG_STREAM << "Attribute type : " << serverAttributeInfo.data_type << endl; 
		running = true;
	}
	return true;
}

Output::~Output()
{
	if(lastReadValues)
	{
		delete lastReadValues;
		lastReadValues = 0;
	}

	if(proxyToData)
	{
		delete proxyToData;
		proxyToData=0;
	}
}
}


