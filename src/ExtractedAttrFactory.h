#ifndef _EXTRACTEDATTRFACTORY_H
#define _EXTRACTEDATTRFACTORY_H

#include <tango.h>
#include "ExtractedAttr.h"

namespace Slicer_ns
{

  class ExtractedAttrFactory : public Tango::LogAdapter
{
protected:
  vector<ExtractedAttr*> attributes;
  Tango::DeviceImpl* device;

public:
  ExtractedAttrFactory(Tango::DeviceImpl* dev):Tango::LogAdapter(dev) { device = dev;}
  ~ExtractedAttrFactory()
  {
    for(int i=0; i<attributes.size(); i++)
    {
      delete attributes[i];
    }
  };
  vector<ExtractedAttr*> getAttributes() {return attributes;}
  Tango::DeviceImpl* getDevice() {return device;}
  ExtractedAttr* getAttribute(string name);

  ExtractedAttr* createAttribute(string inputPath, string outputName, string indexesCouple);

  vector<string> splitString(string input, const char separator);
};

}

#endif //  _EXTRACTEDATTRFACTORY_H
